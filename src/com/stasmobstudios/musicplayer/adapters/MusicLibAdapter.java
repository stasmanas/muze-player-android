/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 23 November, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stasmobstudios.musicplayer.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.stasmobstudios.musicplayer.R;
import com.stasmobstudios.musicplayer.util.MusicUtils;

/**
 * Created by stasmanas on 23/11/14.
 */
public class MusicLibAdapter extends SimpleCursorAdapter {
    public MusicLibAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
    }

    private int mCurrentTrack = -1;

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewBinder binder = super.getViewBinder();
        final int count = mTo.length;
        final int[] from = mFrom;
        final int[] to = mTo;

        for (int i = 0; i < count; i++) {
            final View v = view.findViewById(to[i]);
            if (v != null) {

                boolean bound = false;
                int position = cursor.getPosition();

                if (to[i] == R.id.trackName) {
                    if (v instanceof TextView)
                        ((TextView) v).setTextColor(v.getContext().getResources().getColor(R.color.white));
                    if (mCurrentTrack == position) {
                        if (v instanceof TextView)
                            ((TextView) v).setTextColor(v.getContext().getResources().getColor(R.color.lib_list_title_selected));
                    }
                }

                if (binder != null) {
                    bound = binder.setViewValue(v, cursor, from[i]);
                    if (to[i] == R.id.duration) {
                        if (v instanceof TextView)
                            setViewText((TextView) v, MusicUtils.makeTimeString(context, cursor.getLong(from[i])));
                    }
                }

                if (!bound) {
                    String text = cursor.getString(from[i]);
                    if (text == null) {
                        text = "";
                    }
                    if (to[i] == R.id.duration) {
                        if ("0".equalsIgnoreCase(text))
                            text = "--:--";
                        else
                            text = MusicUtils.makeTimeString(context, (long) cursor.getInt(from[i]) / 1000);
                    }

                    if (v instanceof TextView) {
                        setViewText((TextView) v, text);
                    } else if (v instanceof ImageView) {
                        setViewImage((ImageView) v, text);
                    } else {
                        throw new IllegalStateException(v.getClass().getName() + " is not a " +
                                " view that can be bounds by this SimpleCursorAdapter");
                    }
                }
            }
        }
    }

    public void setCurrentTrack(int mCurrentTrack) {
        this.mCurrentTrack = mCurrentTrack;
    }

    public int getCurrentTrack() {
        return this.mCurrentTrack;
    }
}
