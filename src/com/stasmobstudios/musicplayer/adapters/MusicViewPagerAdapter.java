/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 29 November, 2014
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.stasmobstudios.musicplayer.R;
import com.stasmobstudios.musicplayer.activities.MusicPlayerUI;
import com.stasmobstudios.musicplayer.fragments.EQFragment;
import com.stasmobstudios.musicplayer.fragments.LibraryFragment;
import com.stasmobstudios.musicplayer.fragments.TrackInfoFragment;

import java.util.Locale;

public class MusicViewPagerAdapter extends FragmentPagerAdapter {
    private MusicPlayerUI mMusicPlayerUI;
    private static String TAG = "MusicViewPagerAdapter";

    public MusicViewPagerAdapter(FragmentManager fm, MusicPlayerUI musicPlayerUI) {
        super(fm);
        mMusicPlayerUI = musicPlayerUI;
    }
    
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;

        switch (position) {
            case 0 :
                fragment = new LibraryFragment();
                break;
            case 1 :
                fragment = new TrackInfoFragment();
                break;
            case 2 :
                fragment = new EQFragment();
                break;
            default:
                fragment = new LibraryFragment();
                Log.e(TAG, "ERROR. Out of current views. Giving back Library view");
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return mMusicPlayerUI.getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return mMusicPlayerUI.getString(R.string.title_section2).toUpperCase(l);
            case 2:
                return mMusicPlayerUI.getString(R.string.title_section3).toUpperCase(l);
        }
        return null;
    }
}