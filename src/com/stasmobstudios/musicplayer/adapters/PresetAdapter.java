/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 4 August, 2013
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.stasmobstudios.musicplayer.items.Preset;
import com.stasmobstudios.musicplayer.R;

import java.util.List;


public class PresetAdapter extends ArrayAdapter<Preset> {
    private RelativeLayout presetView;

    public PresetAdapter(Context context, List<Preset> presets) {
        super(context, R.layout.preset_list_item, R.id.preset_name, presets);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        presetView = (RelativeLayout) inflater.inflate(R.layout.preset_list_item, parent, false);
        TextView tvPresetName = ((TextView) presetView.findViewById(R.id.preset_name));
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            tvPresetName.setTextColor(getContext().getResources().getColor(R.color.black));
        if (position < getCount())
            tvPresetName.setText(((Preset) getItem(position)).getPresetName());
        else
            tvPresetName.setText("BUG: Should not appear");

        return presetView;
    }
}
