/*
 * @author Stanislovas Mickus
 * @version 2
 * @date 29 October, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.stasmobstudios.musicplayer.interfaces.PlaybackServiceInterface;
import com.stasmobstudios.musicplayer.interfaces.UIUpdateCallbacks;
import com.stasmobstudios.musicplayer.services.ConstantsPlayBack;

public class PlayBackCommandsReceiver extends BroadcastReceiver {
    private PlaybackServiceInterface mPlaybackService;
    private UIUpdateCallbacks mUIUpdateCallbacks;
    private String CLASS_NAME;

    public PlayBackCommandsReceiver(UIUpdateCallbacks uiUpdateCallbacks, PlaybackServiceInterface playbackService) {
        this.mUIUpdateCallbacks = uiUpdateCallbacks;
        this.mPlaybackService = playbackService;
        CLASS_NAME = this.getClass().getSimpleName().toUpperCase();
    }
    /**
     *
     * This method is called by the system when a broadcast Intent is matched by this class'
     * intent filters
     *
     * @param context An Android context
     * @param intent The incoming broadcast Intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        /**
         * Gets the status from the Intent's extended data, and chooses the appropriate action
         */
        switch (intent.getIntExtra(ConstantsPlayBack.EXTENDED_PLAYBACK_COMMAND,
                ConstantsPlayBack.UKNOWN_STATE)) {

            case ConstantsPlayBack.PLAY_PAUSE_CMD:
                mUIUpdateCallbacks.updatePlayingSate(mPlaybackService.mediaPlayPause());

                if (ConstantsPlayBack.LOGD) {
                    Log.d(CLASS_NAME, "State: PLAY_PAUSE_CMD");
                }
                break;
            case ConstantsPlayBack.PREV_CMD:
                mUIUpdateCallbacks.updatePlayingSate(mPlaybackService.mediaPrev());

                if (ConstantsPlayBack.LOGD) {
                    Log.d(CLASS_NAME, "State: PREV_CMD");
                }
                break;
            case ConstantsPlayBack.NEXT_CMD:
                mUIUpdateCallbacks.updatePlayingSate(mPlaybackService.mediaNext());

                if (ConstantsPlayBack.LOGD) {
                    Log.d(CLASS_NAME, "State: NEXT_CMD");
                }
                break;
            default:
                Log.e(CLASS_NAME, "State: UKNOWN_STATE");
                break;
        }
    }
}

