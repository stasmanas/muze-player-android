/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 13 December, 2013
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import com.stasmobstudios.musicplayer.activities.MusicPlayerUI;
import com.stasmobstudios.musicplayer.interfaces.PlaybackServiceInterface;


public class RemoteControlReceiver extends BroadcastReceiver {
        @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            // handle media button intent here by reading contents
            // of EXTRA_KEY_EVENT to know which key was pressed
            KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
            PlaybackServiceInterface playbackService = MusicPlayerUI.getMusicPlayerActivity().getPlaybackSrevice();

            if ( KeyEvent.KEYCODE_VOLUME_UP == keyEvent.getAction() || KeyEvent.KEYCODE_VOLUME_DOWN == keyEvent.getAction() || KeyEvent.KEYCODE_VOLUME_MUTE == keyEvent.getAction() ) {
                MusicPlayerUI.getMusicPlayerActivity().refreshVolume();
            } else if (KeyEvent.KEYCODE_MEDIA_NEXT == keyEvent.getAction() && playbackService != null) {
                playbackService.mediaNext();
            } else if (KeyEvent.KEYCODE_MEDIA_PREVIOUS == keyEvent.getAction() && playbackService != null) {
                playbackService.mediaPrev();
            } else if (KeyEvent.KEYCODE_MEDIA_PLAY == keyEvent.getAction() && playbackService != null) {
                if (!playbackService.isMediaPlaying())
                    playbackService.mediaPlayPause();
            } else if (KeyEvent.KEYCODE_MEDIA_PAUSE == keyEvent.getAction() && playbackService != null) {
                if (playbackService.isMediaPlaying())
                    playbackService.mediaPlayPause();
            } else if (KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE == keyEvent.getAction() && playbackService != null) {
                playbackService.mediaPlayPause();
            }
        }
    }
}
