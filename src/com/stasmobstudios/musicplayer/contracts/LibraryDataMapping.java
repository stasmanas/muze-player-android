/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 9 November, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.contracts;

import com.stasmobstudios.musicplayer.R;

public class LibraryDataMapping {
    /**
     *     Track info mapping
     */
    public static final String[] mTrackFromColumns = {
            MusicDataProviderContract.TRACK_NAME,
            MusicDataProviderContract.ARTIST_NAME,
            MusicDataProviderContract.TRACK_DURATION
    };
    public static final int[] mTrackToFields = {
            R.id.trackName,
            R.id.artistAlbumInfo,
            R.id.duration
    };

    /**
     *      Album info mapping
     */
    public static final String[] mAlbumFromColumns = {
            MusicDataProviderContract.ALBUM_NAME,
    };
    public static final int[] mAlbumToFields = {
            R.id.trackName
    };

    /**
     *     Artist info mapping
     */
    public static final String[] mArtistFromColumns = {
            MusicDataProviderContract.ARTIST_NAME
    };
    public static final int[] mArtistToFields = {
            R.id.trackName
    };
}
