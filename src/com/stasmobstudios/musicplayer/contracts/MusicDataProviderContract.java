/*
 * @author Stanislovas Mickus
 * @version 5
 * @date 8 November, 2013
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.contracts;

import android.net.Uri;
import android.provider.MediaStore;

public final class MusicDataProviderContract {
    /**
     *      Music metadata fields
     */
    public static final String TRACK_NAME = MediaStore.Audio.Media.TITLE;
    public static final String TRACK_ID = MediaStore.Audio.Media._ID;
    public static final String DATA = MediaStore.Audio.Media.DATA;
    public static final String ALBUM_NAME = MediaStore.Audio.Media.ALBUM;
    public static final String ALBUM_ID = MediaStore.Audio.Media.ALBUM_ID;
    public static final String ARTIST_NAME = MediaStore.Audio.Media.ARTIST;
    public static final String ARTIST_ID = MediaStore.Audio.Media.ARTIST_ID;
    public static final String TRACK_DURATION = MediaStore.Audio.Media.DURATION;

    /**
     *      The media store provider
     */
    public static final Uri CONTENT_URI = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

    /**
     *      Music projections
     */
    public static String[] PROJECTION_ALL_TRACKS = {"*"};
    public static String[] PROJECTION_ALL_ALBUMS = {"DISTINCT ALBUM_ID, " + ALBUM_NAME + ", ROWID _id"};
    public static String[] PROJECTION_ALL_ARTIST = {"DISTINCT ARTIST_ID, " + ARTIST_NAME + ", ROWID _id"};

    /**
     *      Music selections
     */
    public static String SELECTION_MUSIC = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND " + MediaStore.Audio.Media.ALBUM_ID + " LIKE ? AND " + MediaStore.Audio.Media.ARTIST_ID + " LIKE ?";
    public static String SELECTION_MUSIC_ALBUMS = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND " + MediaStore.Audio.Media.ARTIST_ID + " LIKE ?";
    public static String SELECTION_MUSIC_ARTISTS = MediaStore.Audio.Media.IS_MUSIC + " != 0";

}
