/*
 * @author Stanislovas Mickus
 * @version 2
 * @date 29 October, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.interfaces;

import java.io.IOException;

public interface PlaybackServiceInterface {

    /**
     *  Tracks playback control  methods
     */
    public boolean mediaPlayPause();

    public boolean mediaPrev();

    public boolean mediaNext();

    public boolean mediaPlay(int trackID) throws IOException;

    public boolean mediaSeekToProcent(int procent);

    // Set playback track list
    public void setTrackDataArray(String[] trackDataArray);

    // Set effect strength for playback engine
    public void setAuxEffectSendLevel(float level);

    /**
     *  Set/get current track number
     */
    public void setCurrentTrackNo(int currentTrackNo);

    public int getCurrentTrackNo();

    /**
     *  Get information about playback state
     */
    public boolean isPrepared();

    public boolean isMediaPlaying();

    public int getAudioSessionID();

    /**
     *  Track information retrieval methods
     */
    public int getTrackDuration();

    public int getTrackCurrentTime();

    public String getTrackArtist();

    public String getTrackAlbum();

    public String getTrackName();

    public byte[] getAlbumArtwork();

    // Register UI update callback
    public void initControlUICallbacks(UIUpdateCallbacks uiUpdateCallbacks);
}
