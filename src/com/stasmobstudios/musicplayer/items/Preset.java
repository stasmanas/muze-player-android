/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 4 August, 2013
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.items;

public class Preset {
    private String presetName;
    private int presetID;
    private boolean mRestoreSlidesState = true;

    public Preset(String presetName, int presetID) {
        this.presetName = presetName;
        this.presetID = presetID;
    }

    @Override
    public String toString() {
        return presetName;
    }

    //Getters
    public String getPresetName() {
        return presetName;
    }

    public int getPresetID() {
        return presetID;
    }

    //Setters
    public void setPresetName(String presetName) {
        this.presetName = presetName;
    }

    public void setPresetID(int presetID) {
        this.presetID = presetID;
    }

    public boolean isRestoreSlidesState() {
        return mRestoreSlidesState;
    }

    public void setRestoreSlidesState(boolean mRestoreSlidesState) {
        this.mRestoreSlidesState = mRestoreSlidesState;
    }
}