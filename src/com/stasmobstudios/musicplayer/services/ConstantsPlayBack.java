/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 29 October, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.services;

/**
 * Playback service protocol
 */
public class ConstantsPlayBack {
    // Defines a custom Intent action
    public static final String BROADCAST_PLAYBACK_COMMANDS = "com.stasmobstudios.musicplayer.BROADCAST";
    public static final String EXTENDED_PLAYBACK_COMMAND = "com.stasmobstudios.musicplayer.PLAYBACK_COMMAND";

    //Playback commands from notification area
    public static final int PLAY_PAUSE_CMD = 0;
    public static final int NEXT_CMD = 1;
    public static final int PREV_CMD = 2;

    public static final int UKNOWN_STATE = 1000;
    public static final boolean LOGD = true;

}
