/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 17 December, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stasmobstudios.musicplayer.util;

public class ConstantsGUIUpdate
{
    // EQ view update items
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT = "bundle_update_eq_fragment";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_DATA = "bundle_update_eq_fragment_data";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_VOL = "bundle_update_eq_fragment_vol";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_BASS = "bundle_update_eq_fragment_bass";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_VIRT = "bundle_update_eq_fragment_virt";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_PRESET = "bundle_update_eq_fragment_preset";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_EQ1 = "bundle_update_eq_fragment_eq1";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_EQ2 = "bundle_update_eq_fragment_eq2";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_EQ3 = "bundle_update_eq_fragment_eq3";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_EQ4 = "bundle_update_eq_fragment_eq4";
    public static final String BUNDLE_UPDATE_EQ_FRAGMENT_EQ5 = "bundle_update_eq_fragment_eq5";

}
