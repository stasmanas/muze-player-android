/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 18 September, 2013
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.viewgroups;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;


public class StopSwipeViewPager extends android.support.v4.view.ViewPager {
    private boolean lockEnabled;

    public StopSwipeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.lockEnabled = false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.lockEnabled) {
            return false;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.lockEnabled) {
            return false;
        }

        return super.onInterceptTouchEvent(event);
    }

    public void setLockEnabled(boolean enabled) {
        this.lockEnabled = enabled;
    }
}
