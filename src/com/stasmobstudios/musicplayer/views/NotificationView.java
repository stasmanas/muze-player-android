/*
 * @author Stanislovas Mickus
 * @version 1
 * @date 26 November, 2014
 *
 *  Copyright (C) 2013 Stanislovas Mickus
 *
 * Licensed under the GNU General Public License, Version 3 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.gnu.org/copyleft/gpl.html
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.stasmobstudios.musicplayer.views;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.widget.RemoteViews;

import com.stasmobstudios.musicplayer.R;
import com.stasmobstudios.musicplayer.activities.MusicPlayerUI;
import com.stasmobstudios.musicplayer.services.ConstantsPlayBack;


public class NotificationView extends RemoteViews {
    private NotificationManager mNM;
    private int NOTIFICATION = R.string.playback_service_started;
    public Notification mNotification;
    private Context mContext;
    private Bitmap mBitmapPlay, mBitmapPlayPause;

    public NotificationView(Context context , String packageName) {
        // For Gingerbread select notification without controls
        super(packageName, (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) ? R.layout.music_control_panel_in_notification_area : R.layout.music_control_panel_in_notification_area_gingerbeard);

        mContext = context;
        // Initialize playback controls for Honeycomb and above
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mBitmapPlay = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.notification_play);
            mBitmapPlayPause = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.notification_playpause);

            // Register play / pause command intent
            Intent playPauseIntent = new Intent();
            playPauseIntent.setAction(ConstantsPlayBack.BROADCAST_PLAYBACK_COMMANDS);
            playPauseIntent.addCategory(Intent.CATEGORY_DEFAULT);
            playPauseIntent.putExtra(ConstantsPlayBack.EXTENDED_PLAYBACK_COMMAND, ConstantsPlayBack.PLAY_PAUSE_CMD);
            this.setOnClickPendingIntent(R.id.action_notification_playpause, PendingIntent.getBroadcast(mContext, 1, playPauseIntent, 0));
            // Register play prev command intent
            Intent pervCMDIntent = new Intent();
            pervCMDIntent.setAction(ConstantsPlayBack.BROADCAST_PLAYBACK_COMMANDS);
            pervCMDIntent.addCategory(Intent.CATEGORY_DEFAULT);
            pervCMDIntent.putExtra(ConstantsPlayBack.EXTENDED_PLAYBACK_COMMAND, ConstantsPlayBack.PREV_CMD);
            this.setOnClickPendingIntent(R.id.action_notification_prev, PendingIntent.getBroadcast(mContext, 2, pervCMDIntent, 0));
            // Register play next command intent
            Intent nextCMDIntent = new Intent();
            nextCMDIntent.setAction(ConstantsPlayBack.BROADCAST_PLAYBACK_COMMANDS);
            nextCMDIntent.addCategory(Intent.CATEGORY_DEFAULT);
            nextCMDIntent.putExtra(ConstantsPlayBack.EXTENDED_PLAYBACK_COMMAND, ConstantsPlayBack.NEXT_CMD);
            this.setOnClickPendingIntent(R.id.action_notification_next, PendingIntent.getBroadcast(mContext, 3, nextCMDIntent, 0));
        }

        mNM = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    // Setup and start notification
    public void initNotification(CharSequence notificationText) {
        mNotification = new Notification();
        mNotification.icon = R.drawable.ic_launcher;
        mNotification.tickerText = notificationText;
        mNotification.when = System.currentTimeMillis();
        mNotification.contentView = this;
        Intent startIntent = new Intent(mContext, MusicPlayerUI.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingContentIntent = PendingIntent.getActivity(mContext, 0, startIntent, 0);
        mNotification.contentIntent = pendingContentIntent;

        mNM.notify(NOTIFICATION, mNotification);
    }

    public void cancelNotification() {
        mNM.cancel(NOTIFICATION);
    }

    // Update track information in notification
    public void updateTrackInfo(String trackName, String albumName, String artisName) {
        this.setTextViewText(R.id.notification_track_name, trackName);
        this.setTextViewText(R.id.notification_artis_album_name, albumName + " by " + artisName);

        mNM.notify(NOTIFICATION, mNotification);
    }

    // Set album artwork in notification
    public void updateAlbumArtwork(Bitmap bitmapAlbumArtwork) {
        if (bitmapAlbumArtwork != null)
            this.setImageViewBitmap(R.id.notification_image_view, scaleDownBitmap(bitmapAlbumArtwork, 50, mContext));
        else
            this.setImageViewBitmap(R.id.notification_image_view, scaleDownBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_launcher), 50, mContext));

        mNM.notify(NOTIFICATION, mNotification);
    }

    // Update play/pause control state
    public void updatePlayPause(boolean isPlaying) {
        //Buttons are not available in API < 11
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            return;

        if (isPlaying)
            this.setImageViewBitmap(R.id.action_notification_playpause, mBitmapPlayPause);
        else
            this.setImageViewBitmap(R.id.action_notification_playpause, mBitmapPlay);
        mNM.notify(NOTIFICATION, mNotification);
    }

    // Scale bitmap to be suitable for notification
    public static Bitmap scaleDownBitmap(Bitmap photo, int newHeight, Context context) {

        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        int h= (int) (newHeight * densityMultiplier);
        int w= (int) (h * photo.getWidth() / ((double) photo.getHeight()));

        photo=Bitmap.createScaledBitmap(photo, w, h, true);

        return photo;
    }
}
